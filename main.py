import asyncio as aio
import RPi.GPIO as GPIO
import spidev
import smbus


async def connected_cb(reader, writer):
    while True:
        data = await reader.readline()
        print(f"got message: {data.decode()}")
        writer.write(b"response!")
        await writer.drain()


# start the 
async def start_server():
    # start server
    s = await aio.start_server(connected_cb, host= "0.0.0.0", port="8080")
    async with s:
        await s.serve_forever()

# main program entry
async def main():
    # set the gpio naming mode
    GPIO.setmode(GPIO.BCM)
    # configure as output
    GPIO.setup(21, GPIO.OUT)

    # start server
    srv = aio.create_task(start_server())

    
    # spi = spidev.SpiDev()
    # spi.open(0, 0)
    # spi.max_speed_hz=1000000
    # to_send = [0xaa, 0xff, 0xaa]

    # i2c = smbus.SMBus(1)
    # will raise an exception
    # i2c.write_byte_data(0x1, 0x1, 0x80)


    # endless loop of madness
    for i in range(20):
        # perform a spi transfer
        # spi.writebytes(to_send)
        # sleep for one second
        await aio.sleep(0.5)
        # bring high
        GPIO.output(21, GPIO.HIGH)

        # perform a spi transfer
        # spi.writebytes(to_send)
        # sleep for one second
        await aio.sleep(0.5)
        # bring low
        GPIO.output(21, GPIO.LOW)
        print(f"iteration: {i}")
    
    await srv

aio.run(main())
